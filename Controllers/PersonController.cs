﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularFun.Models;

namespace AngularFun.Controllers
{
    public class PersonController : ApiController
    {
        private AngularPeopleEntities db = new AngularPeopleEntities();

        // GET: api/Person
        public IEnumerable<Person> Get()
        {
            List<Person> listOfPeople = new List<Person>(); 
            foreach (Person person in db.Persons)
            {
                listOfPeople.Add(person);
            }
            return listOfPeople;

            //return new List<Person>
            //{
            //    new Person("Asa", 39),
            //    new Person("Rhys", 22),
            //    new Person("Jason", 23),
            //    new Person("Leon", 30),
            //    new Person("Helen", 24),
            //    new Person("Phoebe", 21),
            //    new Person("Nathan", 35)
            //};
        }

        // GET: api/Person/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Person
        public void Post(Person person)
        {
            db.Persons.Add(person);
            db.SaveChanges();
        }

        // PUT: api/Person/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Person/5
        public void Delete(int id)
        {
        }
    }
}
