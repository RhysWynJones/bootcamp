﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AngularFun
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //turn off xml by default will result in json in web browser
            config
                .Formatters
                .JsonFormatter
                .SupportedMediaTypes
                .Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
        }
    }
}
